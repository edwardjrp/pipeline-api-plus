FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7

LABEL maintainer="Edward Rodriguez <Edward.Rodriguez@rackspace.com>"

ENV APP_MODULE "app.main:api"

COPY ./app /app/app/
COPY ./Pipfile* /app/

# Remove image prepackaged filed
RUN rm ./main.py ./prestart.sh && \
    pip install pipenv && \
    pipenv install --system --deploy --ignore-pipfile
