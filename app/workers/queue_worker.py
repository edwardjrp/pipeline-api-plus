from app.services import queue
from app.config import settings

if __name__ == "__main__":
    print("Bootstraping beanstalked worker...")
    queue.process_tube(settings.GITHUB_TUBE)
    print("Exiting beanstalked worker...")
