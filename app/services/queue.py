import greenstalk
from app.config.settings import QUEUE_HOST, QUEUE_PORT, BUILDCONFIGS_URL, BC_TRIGGER_SECRET
import json
import requests
import urllib3



def connect_queue(tube: str, watch_tube=False):
    if watch_tube:
        return greenstalk.Client(host=QUEUE_HOST, port=QUEUE_PORT, use=tube, watch=tube)

    return greenstalk.Client(host=QUEUE_HOST, port=QUEUE_PORT, use=tube)


def process_tube(tube: str):
    print("Connecting to beantaslked host: {} on port: {}".format(QUEUE_HOST, QUEUE_PORT))    
    queue = connect_queue(tube, watch_tube=True)
    print("Watching and using tube: {}".format(tube))
    while True:
        job = queue.reserve()
        if job is not None:
            try:
                content = json.loads(job.body)
                print("Processing git push {}".format(content.__str__()))
                git_repo = content["repository"]["full_name"]
                git_repo = git_repo.replace("/", "-")
                branch = content["repository"]["master_branch"]

                pipeline_name = "content-{}-pipeline".format(git_repo)
                print("Kicking off openshift pipeline: {}".format(pipeline_name))
                print("Branch: {}".format(branch))

                # Openshift build kick off code goes below
                kick_build(pipeline_name)
                
                # After kick success delete job
                queue.delete(job)
            except greenstalk.BeanstalkdError as e:
                print("An error occurred while worker was processing a job")
                print("Job dump: {}".format(job.body))
                print("Error dump: {}".format(e))
                queue.release(job)
                raise



# NOTES: BC_TRIGGER_SECRET is the trigger secret configured as an environment variable
# NOTES: for every dynamic pipeline. Its a generic random token, the same for all.
# NOTES: Please do not confuse the term secret with a way to see things more secursely
# NOTES: This simply adds uniqueness to the triggering url of the buildconfig and doesnt
# NOTES: provide any security.
def kick_build(build_config_name):
    sa_token = get_pod_token()
    if len(sa_token) == 0:
        raise "POD token is invalid or unavailable. This prevents http requests to the cluster API."
    
    headers = {
        'Content-Type': 'application/json',
        'X-GitHub-Event': 'push',
        'Authorization': "Bearer {}".format(sa_token)
    }    
    # Disable ssl warnings
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    url = "{}/{}/webhooks/{}/generic".format(BUILDCONFIGS_URL, build_config_name, BC_TRIGGER_SECRET)
    result = requests.post(url, headers=headers, verify=False, data=json.dumps({}))
    print(result)


def get_pod_token():
    # This is where k8s/openshift place pod SA tokens, should this be dynamic?, to be continued
    with open('/var/run/secrets/kubernetes.io/serviceaccount/token', 'r') as f:
        token = f.read()
    return token
