from app.models.push import GithubPush
from app.models.responses import GithubPushOutput
from app.config.settings import GITHUB_TUBE
from app.services.queue import connect_queue


def queue_github_push(GithubPush):
    try:
        q = connect_queue(GITHUB_TUBE)
        q.put(GithubPush.json())
    except:
        # TODO: Catch the most common error specifically that may happen and log it
        # Reraising the exception on purpose for the flow to brake when it happens
        raise

    return GithubPushOutput(data=GithubPush)
