import pytest
from starlette.testclient import TestClient
from app.main import api
import os


@pytest.fixture()
def api_client(scope="module"):
    """
    Make a 'test rest client' fixture available to test cases.
    """
    return TestClient(api)


@pytest.fixture()
def github_push_event(scope="module"):
    """
    Make a 'github push' event json object fixture available to test cases.
    """
    return {
        "ref": "refs/heads/master",
        "repository": {
            "id": 666,
            "name": "docs-developer-blog",
            "full_name": "rackerlabs/docs-developer-blog",
            "url": "https://github.com/rackerlabs/docs-developer-blog",
            "master_branch": "master",
        },
    }
