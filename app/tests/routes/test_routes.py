from starlette.status import HTTP_202_ACCEPTED, HTTP_401_UNAUTHORIZED, HTTP_200_OK
from app.models.responses import QueueStatus


def test_root_path(api_client):
    response = api_client.get("/")
    content = response.json()
    assert response.status_code == HTTP_200_OK
    assert content["service"] == "pipeline-api-plus"


def test_kick_pipeline(api_client, github_push_event):
    response = api_client.post(
        "/pipeline", headers={"X-GitHub-Event": "push", "Host": "localhost"}, json=github_push_event
    )
    content = response.json()
    assert response.status_code == HTTP_202_ACCEPTED
    assert content["status"] == QueueStatus.ACCEPTED
    
def test_kick_pipeline_unauthorized(api_client, github_push_event):
    response = api_client.post(
        "/pipeline", headers={"X-GitHub-Event": "push", "Host": ""}, json=github_push_event
    )
    content = response.json()
    assert response.status_code == HTTP_401_UNAUTHORIZED
