from app.services import index
from app.config import settings


def test_get_service_info():
    service_info = index.get_service_info()
    assert service_info.service == "pipeline-api-plus"
    assert service_info.version == settings.SERVICE_VERSION
