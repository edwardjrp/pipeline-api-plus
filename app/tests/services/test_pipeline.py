from app.services import pipeline
from app.models.push import GithubPush
from app.models.responses import QueueStatus


def test_queue_github_push(github_push_event):
    push = GithubPush(**github_push_event)
    assert push.ref == "refs/heads/master"
    result = pipeline.queue_github_push(push)
    assert result.status == QueueStatus.ACCEPTED
