from fastapi import FastAPI, HTTPException

from app.config import settings
from app.services import index
from app.models.service_info import ServiceInfo
from app.models.push import GithubPush
from app.services import pipeline
from app.models.responses import GithubPushOutput, Error
from starlette.status import (
    HTTP_202_ACCEPTED,
    HTTP_400_BAD_REQUEST,
    HTTP_501_NOT_IMPLEMENTED,
    HTTP_401_UNAUTHORIZED,
)
from starlette.requests import Request

api = FastAPI(
    title="PIPELINE-API-PLUS API",
    description="Openshift buildconfig enhancement api for content repositories. Part of Deconst platform tooling",
    version=settings.SERVICE_VERSION,
)


@api.get("/", response_model=ServiceInfo)
def get_index():
    return index.get_service_info()


@api.get("/pipeline/{pipeline_name}")
def get_pipeline(pipeline_name: str):
    return {"pipeline_name": pipeline_name, "status": "Running"}


@api.post("/pipeline", response_model=GithubPushOutput, status_code=HTTP_202_ACCEPTED)
def kick_pipeline(push: GithubPush, request: Request):
    error_bag = []

    # Only accept requests from github.com and internal github.com
    # This is not a security setup, its a basic filter to allow traffic only
    # from approve hosts. Better security should be set in a future relase
    host = request.headers.get("Host", None)
    host_list = settings.ALLOWED_HOSTS.replace(" ","").split(",")
    if host not in host_list:
        error = Error(
            status_code=HTTP_401_UNAUTHORIZED,
            msg="Unauthorized host request from host: {}".format(host),
        )
        error_bag.append(error.dict())
        print(error)
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=error_bag)

    github_event = request.headers.get("X-GitHub-Event", None)

    if github_event == "push" and push.repository.master_branch == "master":
        return pipeline.queue_github_push(push)

    if github_event == None or github_event != "push":
        error = Error(
            status_code=HTTP_400_BAD_REQUEST,
            msg="Invalid Header X-GitHub-Event. Only push events allowed in the request payload.",
        )

        error_bag.append(error.dict())
        print(error)
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=error_bag)

    if push.repository.master_branch != "master":
        error = Error(
            status_code=HTTP_400_BAD_REQUEST,
            msg="Only push events to master branch are valid to kick of the pipeline.",
        )
        error_bag.append(error.dict())
        print(error)
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail=error_bag)

    # To handle non-implemented events
    error = Error(
        status_code=HTTP_501_NOT_IMPLEMENTED, msg="Github event not implemented yet."
    )

    error_bag.append(error.dict())
    print(error)
    raise HTTPException(status_code=HTTP_501_NOT_IMPLEMENTED, detail=error_bag)
