from pydantic import BaseModel, Schema, UrlStr


class GithubRepo(BaseModel):
    id: int
    name: str = Schema(..., description="Github repository name")
    full_name: str = Schema(
        ..., description="Github repository full name including org"
    )
    url: UrlStr = Schema(..., description="Github repository url")
    master_branch: str = ""


class GithubPush(BaseModel):
    ref: str = Schema(None, description="Git ref branch, i.e. refs/heads/master")
    repository: GithubRepo = None
