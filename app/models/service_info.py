from pydantic import BaseModel, Schema


class ServiceInfo(BaseModel):
    service: str = "pipeline-api-plus"
    version: str = None
    hash: str = None
    description: str = Schema(
        "Api to enhance pipeline operations within openshift",
        title="Api to enhance pipeline operations within openshift",
    )
