from pydantic import BaseModel, Schema

from app.models.push import GithubPush
from enum import Enum


class QueueStatus(str, Enum):
    ACCEPTED = "ACCEPTED"
    REJECTED = "REJECTED"
    TOO_BIG = "TOO_BIG"


class GithubPushOutput(BaseModel):
    data: GithubPush
    status: QueueStatus = Schema(
        QueueStatus.ACCEPTED,
        description="Status of the payload into the queue for later processing",
    )


class Error(BaseModel):
    status_code: str = None
    msg: str = "An error has occurred"
