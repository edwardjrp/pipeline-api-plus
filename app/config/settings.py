import os

from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret


def getenv_bool(var_name, default_value=False):
    result = default_value
    env_value = os.getenv(var_name)
    if env_value is not None:
        result = env_value.upper() in ("TRUE", "1")
    return result

config_env = Config("app/.env")

DEBUG = config_env("DEBUG", cast=bool, default=False)

# a string of origins separated by commas, e.g: "http://localhost, http://localhost:4200, http://localhost:3000, http://localhost:8080, http://dev.couchbase-project.com, https://stag.couchbase-project.com, https://couchbase-project.com"
# CORS_ORIGINS = os.getenv( "CORS_ORIGINS", "*")
CORS_ORIGINS = config_env("CORS_ORIGINS", default="*")

# Version bumps set from gitlab ci bumps when code changes version should change in a semver fashion
SERVICE_VERSION = config_env("SERVICE_VERSION",default="Unknown")

# First 6 chars of git commit hash
SERVICE_HASH = config_env("SERVICE_HASH", default="Unknown")

SECRET_KEY = config_env("SECRET_KEY", cast=Secret)

# Beanstalkd worker queue config
QUEUE_HOST = config_env("QUEUE_HOST", default="127.0.0.1")
QUEUE_PORT = config_env("QUEUE_PORT", default="11300")
GITHUB_TUBE = config_env("GITHUB_TUBE", default="GITHUB_EVENTS")

# Build condig pipelines and generic trigger secret for queue worker. See queue.py
BUILDCONFIGS_URL= config_env("BUILDCONFIGS_URL", default="https://rsi.rackspace.net/apis/build.openshift.io/v1/namespaces/deconst-cicd/buildconfigs")
BC_TRIGGER_SECRET= config_env("BC_TRIGGER_SECRET", default="123456789")
ALLOWED_HOSTS= config_env("ALLOWED_HOSTS", default="github.com, github.rackspace.com, localhost:8000, localhost")