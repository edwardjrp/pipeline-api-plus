# pipeline-api-plus

`pipeline-api-plus` is a microservice RESTful API to enhance Jenkins build
pipeline integration with OpenShift OKD and build configs. You can use the API
to overcome environment restrictions that might prevent you from directly
triggering builds from a webhook.

## Author / maintainer

[Edward Rodriguez](mailto://Edward.Rodriguez@rackspace.com)

Senior SRE / DevOps, Rackspace

## The service

The `pipeline-api-plus` service enhances how build config pipelines within
OpenShift are triggered to work around limitations to kicking off master
branches in build configs and complexities around PR submission and build
processes.

The API gets webhook incoming branch push and PR payloads and starts pipelines
accordingly. The main parameters about the repository are passed to the
`Jenkinsfile` so that DevOps can improve the pipeline work flow. The engineer
writing the pipeline creates implementation details that push the Git repository
and trigger the pipeline.

## Development dependencies

This project uses `pipenv` to package project dependencies. The `Pipfile` and
`Pipfile.lock` files manage dependency versioning in a more efficient manner
than `requirements.txt`. For more information, see
<https://docs.pipenv.org/en/latest/>.

Quickstart:
``` bash
$ pipenv install
$ pipenv shell
$ python --version
```

## Deployment notes and API documentation

Whenever you deploy the service, the `/docs/` route has the endpoints.
OpenAPI-generated docs and anyone using the service can start here.

## Web hook configuration

After you deploy `pipeline-api-plus`, the webhook URL endpoint for processing
pushes must be configured on the Git repository as you would for any Git
webhook.

This first version of this service includes support for GitHub. Follow-on
releases might include GitLab and other repositories as requested.
