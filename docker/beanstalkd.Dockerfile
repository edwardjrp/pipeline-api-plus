FROM alpine
LABEL maintainer="Edward Rodriguez <Edward.Rodriguez@rackspace.com>"

RUN apk add --no-cache beanstalkd

VOLUME /databeans

EXPOSE 11300
CMD ["/usr/bin/beanstalkd"]
